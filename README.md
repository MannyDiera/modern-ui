# UI-components

## Using this library
**Prerequisites**

1. Make sure your project contains a .npmrc file which points to our private registry on AWS
```
# .npmrc contents
<private-registry-info>
```

```
# Install library
npm install <library-name>
```



## Development
This project is managed by the UI/UX team and team X of developers.

If you have questions/suggestions, contact the following:

- For Design: <email>
- For Development <email>
 
### Install dependencies
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Rollup configuration
Please see the following relevant docs for more detail on rollup configuration.

- [Rollup Docs](https://rollupjs.org/guide/en/)
- Dealing with [Peer Dependencies](https://rollupjs.org/guide/en/#peer-dependencies)


