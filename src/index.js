export { default as HelloWorld } from './components/HelloWorld.vue';
export { default as Header } from './components/Header.vue';
export { default as Footer } from './components/Footer.vue';
export { default as Menu } from './components/Menu.vue';
export { default as NavBar } from './components/NavBar/NavBar.vue';
export { default as CompanyButton } from './components/Company-Button/Company-Button.vue';

import './styles/main.css';
