module.exports = {
	extends: [
		'plugin:vue/recommended',
	],
	rules: {
		'vue/max-attributes-per-line': ['error', {
				'singleline': 3
			}
		]
		// override/add rules settings here, such as:
		// 'vue/no-unused-vars': 'error'
	}
}
