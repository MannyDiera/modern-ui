import vue from 'rollup-plugin-vue';
import renameExtensions from '@betit/rollup-plugin-rename-extensions';
import scss from 'rollup-plugin-scss';
import cleaner from 'rollup-plugin-cleaner';
import commonjs from '@rollup/plugin-commonjs';
import image from '@rollup/plugin-image';

import packageJson from "./package.json";

export default {
  input: 'src/index.js',
  output: [
    {
      format: "cjs",
      dir: packageJson.main,
      sourcemap: false
    },
    {
      format: "esm",
      dir: packageJson.module,
      sourcemap: false
    }
  ],
  external: [ 'vue', 'vue-router', 'vuetify', 'vue-i18n'],
  plugins: [
    cleaner({ targets: [ 'dist' ] }),
    commonjs(),
    image(),
    scss({
      output: 'dist/styles/main.css',
      outputStyle: 'compressed'
    }),
    renameExtensions({
      include: ['**/*.vue'],
      mappings: { '.vue': '.vue.js'}
    }),
    vue()
  ],
  // Prevents bundling and doesn't rename files
  preserveModules: true
}
